-- Some initial data to play with.  These INSERT statements should succeed.
-- Do NOT modify these.
INSERT INTO PossibleGoods VALUES('apple', 'fruit', '28');
INSERT INTO PossibleGoods VALUES('banana', 'fruit', '4');
INSERT INTO PossibleGoods VALUES('carrot', 'vegetable', '14');
INSERT INTO User VALUES(0, 'Jonathan Yu', 'jy178', 'password0');
